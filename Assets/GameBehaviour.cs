﻿using UnityEngine;

namespace Game
{
    public class GameBehaviour : MonoBehaviour
    {
        protected static MainController Main
        {
            get { return MainController.Main; }
        }

        public Vector2 Position
        {
            get { return transform.position; }
        }
    }
}