﻿using System.Linq;
using UnityEngine;

namespace Game
{
    public class MainController : GameBehaviour
    {
        [SerializeField] GameObject Enemy;
        [HideInInspector] public EnemyController EnemyController;
        [HideInInspector] public MainCamera MainCamera;
        [HideInInspector] public Map Map;
        [HideInInspector] public MarineController MarineController;
        [HideInInspector] public Tilemap Tilemap;
        [SerializeField] GameObject Unit;

        public static MainController Main
        {
            get { return GameObject.FindWithTag("GameController").GetComponent<MainController>(); }
        }

        void Awake()
        {
            MainCamera = Camera.main.GetComponent<MainCamera>();

            //Add tilemap
            var mapObj = new GameObject("Map");
            mapObj.transform.position = new Vector3(0, 0, 1);
            Tilemap = mapObj.AddComponent<Tilemap>();
            Map = Tilemap.Create(140, 140);
            Map.Generated += MapGenerated;

            MarineController = gameObject.AddComponent<MarineController>();
            EnemyController = gameObject.AddComponent<EnemyController>();
            gameObject.AddComponent<InputHandler>();
            gameObject.AddComponent<UI>();
        }

        void MapGenerated()
        {
            var count = 0;
            foreach (var tiles in Map.Rooms.Skip(1).Select(c => c.Tiles))
            {
                for (var i = 0; i < tiles.Count; i += 10)
                {
                    var enemy = (GameObject) Instantiate(Enemy, tiles[i].Center, Quaternion.identity);
                    enemy.name = "Enemy " + count;
                    count++;
                }
            }

            Instantiate(Unit, Map.StartingPoint.Center, Quaternion.identity).name = "Marine 1";
            Instantiate(Unit, Map.GetNeighbors(Map.StartingPoint).First(c => c.IsPassable).Center, Quaternion.identity).name = "Marine 2";
            MainCamera.MoveToStart(Map.StartingPoint.Center);
        }
    }
}