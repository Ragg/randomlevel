﻿using System;
using System.Collections.Generic;
using Game.Tiles;
using UnityEngine;

namespace Game
{
    public class FogOfWar : GameBehaviour
    {
        readonly Color32 fogColor = new Color32(0, 0, 0, 100);
        readonly Color32 visibleColor = new Color32(0, 0, 0, 0);
        public HashSet<Point> Visible = new HashSet<Point>();
        Vector2 centerPos;
        Color32[] colors;
        bool[,] discovered;
        MeshFilter filter;
        int height;
        int width;

        void calcLOS()
        {
            const int range = 7;
            var seen = new HashSet<Point>();
            foreach (var marine in Main.MarineController.Marines)
            {
                var t = Main.Tilemap.GetTile(marine.Position);
                var p = t.Point;
                var v = t.Center;
                var tiles = Main.Map.Tiles;
                var xMin = Math.Max(0, p.X - range);
                var yMin = Math.Max(0, p.Y - range);
                var xMax = Math.Min(tiles.GetLength(0), p.X + range);
                var yMax = Math.Min(tiles.GetLength(1), p.Y + range);
                for (var x = xMin; x < xMax; x++)
                {
                    for (var y = yMin; y < yMax; y++)
                    {
                        var tile = new Vector2(x + 0.5f, y + 0.5f);
                        var hit = Physics2D.Linecast(v, tile, Layers.StaticMask);
                        if (hit.collider == null)
                        {
                            seen.Add(new Point(x, y));
                        }
                        else
                        {
                            seen.Add(Main.Tilemap.VecToPoint(hit.point + (tile - v).normalized*0.1f));
                        }
                    }
                }
            }
            Seen(seen);
        }

        void Awake()
        {
            height = Mathf.CeilToInt(Camera.main.orthographicSize*2) + 2;
            width = Mathf.CeilToInt(MainCamera.Aspect*height) + 2;
            centerPos = new Vector2(-1*width/2f, -1*height/2f);

            var mesh = MeshBuilder.BuildMesh(width, height, out colors);

            filter = gameObject.AddComponent<MeshFilter>();
            filter.mesh = mesh;
            var renderer = gameObject.AddComponent<MeshRenderer>();

            var spriteObj = new GameObject();
            var sprite = spriteObj.AddComponent<SpriteRenderer>();
            renderer.sharedMaterial = sprite.sharedMaterial;
            Destroy(spriteObj);

            var sizeX = Main.Map.SizeX;
            var sizeY = Main.Map.SizeY;

            discovered = new bool[sizeX, sizeY];
        }

        void Update()
        {
            calcLOS();
            var center = Main.Tilemap.VecToPoint(Main.MainCamera.Position);
            transform.position = centerPos + new Vector2(center.X, center.Y);
            var xMin = center.X - Mathf.FloorToInt(width/2f);
            var yMin = center.Y - Mathf.FloorToInt(height/2f);
            var xMax = center.X + Mathf.CeilToInt(width/2f);
            var yMax = center.Y + Mathf.CeilToInt(height/2f);
            var i = 0;
            for (var x = xMin; x < xMax; x++)
            {
                for (var y = yMin; y < yMax; y++)
                {
                    if (Visible.Contains(new Point(x, y)))
                    {
                        SetColor(visibleColor, i);
                    }
                    else if (discovered[x, y])
                    {
                        SetColor(fogColor, i);
                    }
                    else
                    {
                        SetColor(Color.black, i);
                    }
                    i++;
                }
            }
            filter.mesh.colors32 = colors;
        }

        void SetColor(Color32 color, int start)
        {
            var vertex = start*4;
            for (var i = vertex; i < vertex + 4; i++)
            {
                colors[i] = color;
            }
        }

        public void Seen(HashSet<Point> points)
        {
            foreach (var p in points)
            {
                discovered[p.X, p.Y] = true;
            }
            Visible = points;
        }
    }
}