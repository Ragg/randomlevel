using System;
using System.Collections.Generic;
using System.Linq;

namespace Game
{
    public struct Point : IEquatable<Point>
    {
        public readonly int X, Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Point Normalized
        {
            get { return new Point(Math.Sign(X), Math.Sign(Y)); }
        }

        public bool Equals(Point p)
        {
            return X == p.X && Y == p.Y;
        }

        static IEnumerable<int> Range(int a, int b)
        {
            var sign = Math.Sign(b - a);
            var i = a;
            yield return i;
            for (i += sign; i != b; i += sign)
            {
                yield return i;
            }
            yield return i;
        }

        public static IEnumerable<Point> Line(Point a, Point b)
        {
            if (a.X != b.X)
            {
                var enumerable = Range(a.X, b.X).Select(c => new Point(c, a.Y)).ToArray();
                return enumerable;
            }
            var @select = Range(a.Y, b.Y).Select(c => new Point(a.X, c)).ToArray();
            return @select;
        }

        public IEnumerable<Point> GetNeighbors(bool diagonal = true)
        {
            yield return new Point(X, Y + 1);
            yield return new Point(X, Y - 1);
            yield return new Point(X + 1, Y);
            yield return new Point(X - 1, Y);
            if (!diagonal)
            {
                yield break;
            }
            yield return new Point(X + 1, Y + 1);
            yield return new Point(X + 1, Y - 1);
            yield return new Point(X - 1, Y + 1);
            yield return new Point(X - 1, Y - 1);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Point))
            {
                return false;
            }
            var p = (Point) obj;
            return X == p.X && Y == p.Y;
        }

        public override int GetHashCode()
        {
            var hash = 13;
            hash = (hash*7) + X;
            hash = (hash*7) + Y;
            return hash;
        }

        public override string ToString()
        {
            return string.Format("Point: ({0},{1})", X, Y);
        }

        public static bool operator ==(Point a, Point b)
        {
            // Return true if the fields match:
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Point a, Point b)
        {
            return !(a == b);
        }

        public static Point operator *(Point a, int b)
        {
            return new Point(a.X*b, a.Y*b);
        }

        public static Point operator +(Point a, Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        public static Point operator -(Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }
    }
}