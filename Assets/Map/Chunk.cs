﻿using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using Game.Tiles;
using UnityEngine;

namespace Game
{
    public class Chunk : GameBehaviour
    {
        int SizeX;
        int SizeY;
        Color32[] colors;
        bool dirty;
        MeshFilter filter;
        Tile[,] tiles;

        Tile BottomLeft
        {
            get { return tiles[0, 0]; }
        }

        Tile BottomRight
        {
            get { return tiles[tiles.GetUpperBound(0), 0]; }
        }

        Tile TopLeft
        {
            get { return tiles[0, tiles.GetUpperBound(1)]; }
        }

        Tile TopRight
        {
            get { return tiles[tiles.GetUpperBound(0), tiles.GetUpperBound(1)]; }
        }

        void Awake()
        {
            gameObject.layer = Layers.Static;
        }

        void Update()
        {
            UpdateColors();
        }

        public Chunk Create(Transform parent, int sizeX, int sizeY, int posX, int posY)
        {
            SizeX = sizeX;
            SizeY = sizeY;
            tiles = new Tile[sizeX, sizeY];

            gameObject.transform.parent = parent;
            gameObject.transform.localPosition = new Vector2(posX, posY);
            gameObject.name = "Chunk";

            var mesh = MeshBuilder.BuildMesh(sizeX, sizeY, out colors);

            filter = gameObject.AddComponent<MeshFilter>();
            filter.mesh = mesh;
            var renderer = gameObject.AddComponent<MeshRenderer>();
            var spriteObj = new GameObject();
            var sprite = spriteObj.AddComponent<SpriteRenderer>();
            renderer.sharedMaterial = sprite.sharedMaterial;
            Destroy(spriteObj);
            return this;
        }

        void UpdateColors()
        {
            if (dirty)
            {
                filter.mesh.colors32 = colors;
                dirty = false;
            }
        }

        public void SetTile(Tile tile, int x, int y)
        {
            SetColor(tile.Color, x, y);
            tiles[x, y] = tile;
            dirty = true;
        }

        public void SetColor(Color32 color, int x, int y)
        {
            var vertex = TilePos(x, y)*4;
            for (var i = vertex; i < vertex + 4; i++)
            {
                colors[i] = color;
            }
            dirty = true;
        }

        int TilePos(int x, int y)
        {
            return x*SizeY + y;
        }

        public void GenerateColliders()
        {
            var clipper = new Clipper();
            var inVerts = new List<List<IntPoint>>();
            for (var x = 0; x < tiles.GetLength(0); x++)
            {
                for (var y = 0; y < tiles.GetLength(1); y++)
                {
                    var tile = tiles[x, y];
                    if (tile.IsPassable)
                    {
                        continue;
                    }
                    inVerts.Add(new List<IntPoint>
                    {
                        new IntPoint(tile.X, tile.Y),
                        new IntPoint(tile.X, tile.Y + 1),
                        new IntPoint(tile.X + 1, tile.Y + 1),
                        new IntPoint(tile.X + 1, tile.Y)
                    });
                }
            }
            clipper.AddPaths(inVerts, PolyType.ptSubject, true);
            var outVerts = new List<List<IntPoint>>();
            clipper.Execute(ClipType.ctUnion, outVerts);
            var vecs = outVerts.Select(c => c.Select(d => new Vector2(d.X, d.Y) - (Vector2) transform.position).ToArray()).ToList();
            var coll = gameObject.AddComponent<PolygonCollider2D>();
            coll.pathCount = vecs.Count;
            for (var i = 0; i < vecs.Count; i++)
            {
                coll.SetPath(i, vecs[i]);
            }
            coll.isTrigger = true;
        }
    }
}