﻿using Game.Tiles;
using UnityEngine;

namespace Game
{
    public class Tilemap : GameBehaviour
    {
        public FogOfWar FOW;
        int chunkSizeX;
        int chunkSizeY;
        Chunk[,] chunks;
        Map map;

        public Tile BottomLeft
        {
            get { return map.Tiles[0, 0]; }
        }

        public Tile BottomRight
        {
            get { return map.Tiles[map.Tiles.GetUpperBound(0), 0]; }
        }

        public Tile TopLeft
        {
            get { return map.Tiles[0, map.Tiles.GetUpperBound(1)]; }
        }

        public Tile TopRight
        {
            get { return map.Tiles[map.Tiles.GetUpperBound(0), map.Tiles.GetUpperBound(1)]; }
        }

        public Point VecToPoint(Vector2 v)
        {
            return new Point(Mathf.FloorToInt(v.x), Mathf.FloorToInt(v.y));
        }

        public Vector2 PointToVec(Point p)
        {
            return new Vector2(p.X, p.Y);
        }

        public bool InBounds(Vector2 v)
        {
            return map.InBounds(VecToPoint(v));
        }

        public Tile GetTile(Vector2 v)
        {
            var p = VecToPoint(v);
            return map.InBounds(p) ? map.Tiles[p.X, p.Y] : null;
        }

        void SetChunkTile(Tile tile)
        {
            chunks[tile.X/chunkSizeX, tile.Y/chunkSizeY].SetTile(tile, tile.X - tile.X/chunkSizeX*chunkSizeX, tile.Y - tile.Y/chunkSizeY*chunkSizeY);
        }

        void BuildCollider()
        {
            var coll = gameObject.AddComponent<EdgeCollider2D>();
            var inner = new[]
            {
                TopLeft.TopLeft,
                TopRight.TopRight,
                BottomRight.BottomRight,
                BottomLeft.BottomLeft,
                TopLeft.TopLeft
            };
            coll.points = inner;
            coll.isTrigger = true;
        }

        void Awake()
        {
            gameObject.layer = Layers.Static;
        }

        public Map Create(int sizeX, int sizeY)
        {
            map = gameObject.AddComponent<Map>();
            map.TileChanged += SetChunkTile;
            map.Create(sizeX, sizeY);
            //Initialize tilemap chunks
            chunkSizeX = 20;
            chunkSizeY = 20;
            var numChunksX = (sizeX - 1)/chunkSizeX + 1;
            var numChunksY = (sizeY - 1)/chunkSizeY + 1;
            chunks = new Chunk[numChunksX, numChunksY];
            for (var x = 0; x < numChunksX; x++)
            {
                for (var y = 0; y < numChunksY; y++)
                {
                    var chunk = new GameObject().AddComponent<Chunk>();
                    chunks[x, y] = chunk.Create(gameObject.transform, chunkSizeX, chunkSizeY, x*chunkSizeX, y*chunkSizeY);
                    map.Generated += chunks[x, y].GenerateColliders;
                }
            }
            map.Generated += BuildCollider;
            Main.MainCamera.CameraReady += FogOfWar;
            map.Generate();
            return map;
        }

        void FogOfWar()
        {
            var FOWobj = new GameObject("Fog of War");
            FOW = FOWobj.AddComponent<FogOfWar>();
        }
    }
}