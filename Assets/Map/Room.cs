﻿using System;
using System.Collections.Generic;
using Game.Tiles;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
    public class Room : IEquatable<Room>
    {
        public readonly Color32 Color = new Color(Random.value, Random.value, Random.value);
        public readonly List<Tile> Tiles = new List<Tile>();
        readonly int id;

        public Room(int id, Color color)
        {
            Color = color;
            this.id = id;
        }

        public Room(int id)
        {
            this.id = id;
        }

        public bool Equals(Room other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            return id == other.id;
        }

        public static bool operator ==(Room left, Room right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Room left, Room right)
        {
            return !Equals(left, right);
        }

        public override int GetHashCode()
        {
            return id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != GetType())
            {
                return false;
            }
            return Equals((Room) obj);
        }
    }
}