using UnityEngine;

namespace Game
{
    public static class MeshBuilder
    {
        public static Mesh BuildMesh(int sizeX, int sizeY, out Color32[] colors)
        {
            var size = sizeX*sizeY;
            var mesh = new Mesh();
            var vertices = new Vector3[size*4];
            var uv = new Vector2[vertices.Length];
            for (var x = 0; x < sizeX; x++)
            {
                for (var y = 0; y < sizeY; y++)
                {
                    var i = (x*sizeY + y)*4;
                    vertices[i] = new Vector3(x, y, 0);
                    vertices[i + 1] = new Vector3(x, y + 1, 0);
                    vertices[i + 2] = new Vector3(x + 1, y + 1, 0);
                    vertices[i + 3] = new Vector3(x + 1, y, 0);

                    uv[i] = new Vector2(0, 0);
                    uv[i + 1] = new Vector2(0, 1);
                    uv[i + 2] = new Vector2(1, 1);
                    uv[i + 3] = new Vector2(1, 0);
                }
            }
            mesh.vertices = vertices;

            var triangles = new int[size*6];
            for (var i = 0; i < triangles.Length; i += 6)
            {
                var v = i/6*4;
                triangles[i] = v;
                triangles[i + 1] = v + 1;
                triangles[i + 2] = v + 2;
                triangles[i + 3] = v;
                triangles[i + 4] = v + 2;
                triangles[i + 5] = v + 3;
            }
            mesh.triangles = triangles;
            mesh.uv = uv;

            colors = new Color32[vertices.Length];
            for (var i = 0; i < colors.GetLength(0); i++)
            {
                colors[i] = Color.black;
            }
            mesh.colors32 = colors;
            return mesh;
        }
    }
}