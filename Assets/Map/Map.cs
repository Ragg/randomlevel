﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Game.Tiles;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = System.Random;

namespace Game
{
    public class Map : MonoBehaviour
    {
        public readonly List<Room> Rooms = new List<Room>();
        public int[,] Grid;
        public int SizeX, SizeY;
        public Tile StartingPoint;
        public Tile[,] Tiles { get; private set; }


        public bool InBounds(Point p)
        {
            return p.X >= 0 && p.Y >= 0 && p.X < Tiles.GetLength(0) && p.Y < Tiles.GetLength(1);
        }

        public event Action<Tile> TileChanged = delegate { };
        public event Action Generated = delegate { };

        public IEnumerable<Tile> GetNeighbors(Tile t, bool diagonal = true)
        {
            return t.Point.GetNeighbors(diagonal).Where(InBounds).Select(c => GetTile(c));
        }

        public bool IsCorner(Tile t)
        {
            return t.IsPassable && GetNeighbors(t, false).Count(c => !c.IsPassable) > 1;
        }


        public Tile GetTile(Point p)
        {
            return InBounds(p) ? Tiles[p.X, p.Y] : null;
        }

        public void Create(int sizeX, int sizeY)
        {
            SizeX = sizeX;
            SizeY = sizeY;
            Grid = new int[sizeX, sizeY];
            Tiles = new Tile[sizeX, sizeY];
        }

        public void Generate()
        {
            for (var x = 0; x < SizeX; x++)
            {
                for (var y = 0; y < SizeY; y++)
                {
                    SetTile(new Wall(x, y));
                    Grid[x, y] = 100;
                }
            }

            var rects = GenerateRooms();

            foreach (var rect in rects)
            {
                foreach (var tile in rect)
                {
                    SetTile(new Floor(tile[0], tile[1]));
                }
            }

            for (var x = 0; x < Tiles.GetLength(0); x++)
            {
                for (var y = 0; y < Tiles.GetLength(1); y++)
                {
                    FloodFill(x, y);
                    Grid[x, y] = Tiles[x, y].IsPassable ? 10 : 100;
                }
            }
            StartingPoint = Rooms[0].Tiles[0];
            StartCoroutine(MakeTunnels());
        }

        IEnumerator MakeTunnels()
        {
            var sw = new Stopwatch();
            sw.Start();
            var grid = new int[Grid.GetLength(0) - 1, Grid.GetLength(1) - 1];
            for (var x = 0; x < grid.GetLength(0); x++)
            {
                for (var y = 0; y < grid.GetLength(1); y++)
                {
                    var ts = CornerTiles(Tiles[x, y]);
                    if (ts.Where(c => !c.IsPassable).Count(c => GetNeighbors(c).Any(d => d.IsPassable)) > 2)
                    {
                        grid[x, y] = -1;
                    }
                    else if (ts.Any(IsCorner) || ts.All(c => !c.IsPassable) && ts.Any(c => GetNeighbors(c).Any(IsCorner)) && ts.Where(c => !c.IsPassable).SelectMany(c => GetNeighbors(c).Where(d => d.IsPassable)).Count() < 6)
                    {
                        grid[x, y] = 1000;
                    }
                    else if (ts.All(c => c.IsPassable))
                    {
                        grid[x, y] = 10;
                    }
                    else
                    {
                        grid[x, y] = 100;
                    }
                }
            }
            var connected = new Dictionary<Room, HashSet<Room>>();
            foreach (var room in Rooms)
            {
                connected[room] = new HashSet<Room>();
            }
            for (var i = 0; i < Rooms.Count - 1; i++)
            {
                if (i > 0)
                {
                    break;
                }
                var startRoom = Rooms[i];
                var start = (Floor) startRoom.Tiles.First(c => CornerTiles(c).All(d => d.IsPassable && !IsCorner(d)));
                for (var j = i + 1; j < Rooms.Count; j++)
                {
                    var endRoom = Rooms[j];
                    if (connected[startRoom].Contains(endRoom))
                    {
                        continue;
                    }
                    var end = (Floor) endRoom.Tiles.First(c => CornerTiles(c).All(d => d.IsPassable && !IsCorner(d)));
                    var pathfinder = new Pathfinder(grid, false);
                    IEnumerable<Point> path = pathfinder.FindPath(start.X, start.Y, end.X, end.Y);
                    yield return StartCoroutine(BuildTunnels(path.ToArray(), grid, connected));
                }
            }
            for (var x = 0; x < Tiles.GetLength(0); x++)
            {
                for (var y = 0; y < Tiles.GetLength(1); y++)
                {
                    Grid[x, y] = Tiles[x, y].Cost;
                }
            }
            sw.Stop();
            print(sw.Elapsed);
            Generated();
        }


        IEnumerator BuildTunnels(IList<Point> path, int[,] grid, Dictionary<Room, HashSet<Room>> connected)
        {
            //UI.proceed = false;
            while (UI.proceed == false)
            {
                for (var i = 0; i < path.Count - 1; i++)
                {
                    Debug.DrawLine(MainController.Main.Tilemap.PointToVec(path[i]) + new Vector2(1, 1), MainController.Main.Tilemap.PointToVec(path[i + 1]) + new Vector2(1, 1), Color.red);
                }
                yield return null;
            }
            var rooms = new HashSet<Room>();
            for (var i = 0; i < path.Count; i++)
            {
                var p = path[i];
                var ts = CornerTiles(p);

                var tile = ts[0] as Floor;
                if (tile != null)
                {
                    rooms.Add(tile.Room);
                }

                var count = ts.Count(c => c.IsPassable);
                if (count == 1 || count > 2)
                {
                    continue;
                }
                var countPrev = CornerTiles(path[i - 1]).Count(c => c.IsPassable);
                var countNext = CornerTiles(path[i + 1]).Count(c => c.IsPassable);
                if (count == 2 && (countPrev != 4 || countPrev == 4 && countNext == 3))
                {
                    continue;
                }
                foreach (var t in ts.Where(t => !t.IsPassable))
                {
                    SetTile(new Corridor(t.X, t.Y));
                    grid[p.X, p.Y] = 10;
                }
                yield return null;
            }

            foreach (var room in rooms)
            {
                connected[room].UnionWith(rooms);
            }
        }

        Tile[] CornerTiles(Point p)
        {
            Tile[] ts =
            {
                Tiles[p.X, p.Y],
                Tiles[p.X + 1, p.Y],
                Tiles[p.X, p.Y + 1],
                Tiles[p.X + 1, p.Y + 1]
            };
            return ts;
        }

        Tile[] CornerTiles(Tile t)
        {
            return CornerTiles(t.Point);
        }

        void FloodFill(int x, int y, Room room = null)
        {
            if (x < 0 || y < 0 || x >= Tiles.GetLength(0) || y >= Tiles.GetLength(1))
            {
                return;
            }
            var tile = Tiles[x, y] as Floor;
            if (tile == null || tile.Room != null)
            {
                return;
            }
            if (room == null)
            {
                room = new Room(Rooms.Count, new Color32(0, 50, 50, 255));
                //room = new Room(Rooms.Count, Color.blue);
                Rooms.Add(room);
            }
            tile.Room = room;
            room.Tiles.Add(tile);
            SetTile(tile);
            FloodFill(x + 1, y, room);
            FloodFill(x - 1, y, room);
            FloodFill(x, y + 1, room);
            FloodFill(x, y - 1, room);
        }

        int[][][] GenerateRooms()
        {
            var seed = Environment.TickCount;
            //seed = 947097763;
            print(seed);
            var random = new Random(seed);
            const int numRooms = 100;
            const int border = 20;
            var rooms = new int[numRooms][][];
            for (var i = 0; i < numRooms; i++)
            {
                var posX = random.Next(border, SizeX - border);
                var posY = random.Next(border, SizeY - border);
                var w = random.Next(3, 10);
                var h = random.Next(3, 10);
                if (w < 4 && h < 4)
                {
                    i--;
                    continue;
                }
                rooms[i] = new int[w*h][];
                for (var x = 0; x < w; x++)
                {
                    for (var y = 0; y < h; y++)
                    {
                        rooms[i][x*h + y] = new[] {posX + x, posY + y};
                    }
                }
            }
            return rooms;
        }

        void SetTile(Tile tile)
        {
            Tiles[tile.X, tile.Y] = tile;
            Grid[tile.X, tile.Y] = tile.Cost;
            TileChanged(tile);
        }
    }
}