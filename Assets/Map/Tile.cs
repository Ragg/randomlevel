﻿using Game.Geometry;
using UnityEngine;

namespace Game
{
    namespace Tiles
    {
        public abstract class Tile
        {
            public readonly int X;
            public readonly int Y;
            protected readonly Color32 color;

            protected Tile(int x, int y, Color color)
            {
                X = x;
                Y = y;
                this.color = color;
                Cost = 10;
                IsPassable = true;
            }

            public int Cost { get; protected set; }

            public virtual Color32 Color
            {
                get { return color; }
            }

            public bool IsPassable { get; protected set; }

            public Vector2 Vector2
            {
                get { return new Vector2(X, Y); }
            }

            public Vector2 Center
            {
                get { return Vector2 + new Vector2(0.5f, 0.5f); }
            }

            public Point Point
            {
                get { return new Point(X, Y); }
            }

            public Vector2 BottomLeft
            {
                get { return Vector2; }
            }

            public Vector2 BottomRight
            {
                get { return Vector2 + new Vector2(1, 0); }
            }

            public Vector2 TopLeft
            {
                get { return Vector2 + new Vector2(0, 1); }
            }

            public Vector2 TopRight
            {
                get { return Vector2 + new Vector2(1, 1); }
            }

            public override string ToString()
            {
                return GetType().ToString() + Point;
            }

            public Rectangle Rect
            {
                get { return new Rectangle(X, Y, 1, 1); }
            }
        }

        public class Wall : Tile
        {
            public Wall(int x, int y) : base(x, y, new Color32(20, 20, 20, 255))
            {
                Cost = -1;
                IsPassable = false;
            }
        }

        public class Floor : Tile
        {
            public Room Room;

            public Floor(int x, int y) : base(x, y, UnityEngine.Color.white)
            {
            }

            public override Color32 Color
            {
                get { return Room == null ? color : Room.Color; }
            }
        }

        public class Corridor : Tile
        {
            public Corridor(int x, int y) : base(x, y, UnityEngine.Color.white)
            {
            }
        }
    }
}