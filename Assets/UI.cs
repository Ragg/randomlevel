﻿using System;
using Game.Tiles;
using UnityEngine;

namespace Game
{
    class UI : GameBehaviour
    {
        public static bool proceed = true;
        Vector2 selectEnd;
        Vector2 selectStart;
        GUIStyle selectStyle;

        void OnGUI()
        {
            if (Input.GetMouseButton(0))
            {
                var left = selectStart.x;
                var top = Screen.height - selectStart.y;
                var width = selectEnd.x - left;
                var height = Screen.height - selectEnd.y - top;
                var selectRect = new Rect(left, top, width, height);
                GUI.Box(selectRect, GUIContent.none, selectStyle);
            }

            DrawDebugInfo();

            if (GUILayout.Button("Proceed"))
            {
                proceed = true;
            }
        }

        static void DrawDebugInfo()
        {
            GUILayout.Label(Time.realtimeSinceStartup.ToString());
            //Debug info about positions
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var p = Main.Tilemap.VecToPoint(pos);
            var mPos = String.Format("{0} {1}", pos, p);
            GUILayout.Label(mPos);
            //Debug info about screen size
            GUILayout.Label(String.Format("{0}x{1}", Camera.main.pixelWidth, Camera.main.pixelHeight));
        }

        void DragSelect()
        {
            Main.MarineController.Select(Camera.main.ScreenToWorldPoint(selectStart), Camera.main.ScreenToWorldPoint(selectEnd));
        }

        void Awake()
        {
            var selectTexture = new Texture2D(1, 1);
            selectTexture.SetPixel(0, 0, new Color32(100, 100, 100, 100));
            selectTexture.Apply();
            selectStyle = new GUIStyle {normal = {background = selectTexture}};
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                selectStart = Input.mousePosition;
                selectEnd = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                selectEnd = Input.mousePosition;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                DragSelect();
            }
            else if (Input.GetMouseButtonDown(1))
            {
                Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Main.MarineController.Move(pos);
            }
            else if (Input.GetMouseButtonDown(2))
            {
                Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                var t = Main.Tilemap.GetTile(pos);
                print(Main.Map.Grid[t.X, t.Y]);
            }
        }
    }
}