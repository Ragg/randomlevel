﻿using System.Collections;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof (Rigidbody2D))]
    [RequireComponent(typeof (CircleCollider2D))]
    [RequireComponent(typeof (SpriteRenderer))]
    [RequireComponent(typeof (UnitMover))]
    public abstract class Unit : GameBehaviour
    {
        public int Health = 100;
        public UnitMover UnitMover;
        public CircleCollider2D cc2D;
        Color32 color;
        protected Rigidbody2D rb2D;
        protected abstract bool Friendly { get; }

        protected virtual void Awake()
        {
            color = GetComponent<SpriteRenderer>().color;
            cc2D = GetComponent<CircleCollider2D>();
            cc2D.isTrigger = true;
            rb2D = GetComponent<Rigidbody2D>();
            rb2D.isKinematic = true;
            UnitMover = GetComponent<UnitMover>();
            gameObject.layer = Layers.Dynamic;
        }

        public virtual void TakeDamage(int damage)
        {
            Health -= damage;
            if (Health < 0)
            {
                Destroy(gameObject);
                return;
            }
            StartCoroutine(DamageFlash());
        }

        IEnumerator DamageFlash()
        {
            var rend = GetComponent<SpriteRenderer>();
            rend.color = Color.red;
            yield return new WaitForSeconds(0.1f);
            rend.color = color;
        }

        protected bool IsInRange(Unit other, int range)
        {
            return Mathf.Abs(Position.x - other.Position.x) < range && Mathf.Abs(Position.y - other.Position.y) < range;
        }

        public bool IsFriendlyTo(Unit other)
        {
            return Friendly == other.Friendly;
        }
    }
}