﻿namespace Game
{
    public class Conflict
    {
        public readonly UnitMover Other;
        public bool IsMovingAway;

        public Conflict(UnitMover other)
        {
            Other = other;
        }
    }
}