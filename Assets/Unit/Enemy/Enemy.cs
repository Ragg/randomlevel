﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game
{
    public class Enemy : Unit
    {
        public float Speed = 3;
        EnemyController enemyController;
        Marine target;

        protected override bool Friendly
        {
            get { return false; }
        }

        void OnEnable()
        {
            enemyController.Add(this);
        }

        void OnDisable()
        {
            enemyController.Remove(this);
        }

        protected override void Awake()
        {
            base.Awake();
            enemyController = Main.EnemyController;
        }

        IEnumerator Attack()
        {
            while (target != null)
            {
                if ((target.Position - Position).magnitude < 1)
                {
                    target.TakeDamage(20);
                    if (target.Health <= 0)
                    {
                        break;
                    }
                    yield return new WaitForSeconds(1);
                }
                yield return null;
            }
            target = null;
        }

        IEnumerator Pursue()
        {
            yield return new WaitForSeconds(1);
            var oldPos = target.Position;
            UnitMover.MoveTo(target.Position);
            while (target != null)
            {
                if (oldPos != target.Position)
                {
                    oldPos = target.Position;
                    UnitMover.MoveTo(target.Position);
                }
                yield return null;
            }
        }

        void Update()
        {
            GetComponent<Renderer>().enabled = false;
            var marines = Main.MarineController.Marines.Where(c => IsInRange(c, 7));
            foreach (var marine in marines)
            {
                var cast = Physics2D.Linecast(transform.position, marine.Position, Layers.StaticMask);
                if (cast.collider == null)
                {
                    GetComponent<Renderer>().enabled = true;
                    if (target == null)
                    {
                        target = marine;
                        StartCoroutine(Pursue());
                        StartCoroutine(Attack());
                    }
                    break;
                }
            }
        }
    }
}