﻿using System.Collections.Generic;

namespace Game
{
    public class EnemyController : GameBehaviour
    {
        public readonly List<Enemy> Enemies = new List<Enemy>();

        public void Remove(Enemy e)
        {
            Enemies.Remove(e);
        }

        public void Add(Enemy e)
        {
            Enemies.Add(e);
            e.transform.parent = transform;
        }
    }
}