﻿using UnityEngine;

namespace Game
{
    [RequireComponent(typeof (Rigidbody2D))]
    public class Bullet : GameBehaviour
    {
        const float Speed = 5;
        CircleCollider2D cc2D;
        Rigidbody2D rb2D;

        void Awake()
        {
            rb2D = GetComponent<Rigidbody2D>();
            rb2D.isKinematic = true;
        }

        public void Fire(Marine marine, Vector2 tar)
        {
            rb2D.velocity = (tar - Position).normalized*Speed;
            cc2D = gameObject.AddComponent<CircleCollider2D>();
            cc2D.isTrigger = true;
            Physics2D.IgnoreCollision(cc2D, marine.cc2D);
        }

        void FixedUpdate()
        {
            transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z + 360*Time.fixedDeltaTime);
        }

        void OnTriggerEnter2D(Collider2D coll)
        {
            var enemy = coll.gameObject.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.TakeDamage(20);
            }
            Destroy(gameObject);
        }
    }
}