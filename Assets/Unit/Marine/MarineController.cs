﻿using System.Collections.Generic;
using System.Linq;
using Game.Tiles;
using UnityEngine;

namespace Game
{
    public class MarineController : GameBehaviour
    {
        public List<Marine> Marines = new List<Marine>();
        public List<Marine> selected = new List<Marine>();

        public void Select(Vector2 from, Vector2 to)
        {
            var units = Physics2D.OverlapAreaAll(from, to).Select(c => c.GetComponent<Marine>()).Where(c => c != null).ToList();
            selected = units;
        }

        public void Stop()
        {
            foreach (var marine in selected)
            {
                marine.Stop();
            }
        }

        public void Move(Vector2 pos)
        {
            if (!Main.Tilemap.InBounds(pos) || !Main.Tilemap.GetTile(pos).IsPassable)
            {
                return;
            }
            var tiles = new List<Tile> {Main.Tilemap.GetTile(pos)};
            var count = 0;
            while (tiles.Count < selected.Count)
            {
                foreach (var neighbor in Main.Map.GetNeighbors(tiles[count]).Where(c => c.IsPassable))
                {
                    if (!tiles.Contains(neighbor))
                    {
                        tiles.Add(neighbor);
                    }
                }
                count++;
            }
            for (var i = 0; i < selected.Count; i++)
            {
                selected[i].MoveTo(tiles[i].Center);
            }
        }
    }
}