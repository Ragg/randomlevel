﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game
{
    public class Marine : Unit
    {
        [SerializeField] GameObject bullet;
        [SerializeField] Sprite circle;
        MarineController marineController;
        GameObject moveCircle;
        GameObject selectCircle;

        protected override bool Friendly
        {
            get { return true; }
        }

        void OnEnable()
        {
            marineController.Marines.Add(this);
        }

        void OnDisable()
        {
            marineController.Marines.Remove(this);
            marineController.selected.Remove(this);
        }

        protected override void Awake()
        {
            base.Awake();
            marineController = Main.MarineController;
            moveCircle = new GameObject();
            moveCircle.transform.localScale = new Vector3(0.9f, 0.9f);
            moveCircle.name = "Move Circle";
            moveCircle.AddComponent<SpriteRenderer>().sprite = circle;
            moveCircle.GetComponent<SpriteRenderer>().color = Color.green;
            selectCircle = (GameObject) Instantiate(moveCircle);
            selectCircle.name = "Select Circle";
            selectCircle.transform.parent = transform;
            selectCircle.transform.localPosition = Vector2.zero;
            selectCircle.GetComponent<SpriteRenderer>().color = Color.blue;
        }

        void OnDestroy()
        {
            Destroy(moveCircle);
        }

        void Update()
        {
            DrawUnitCircles();
        }


        void Start()
        {
            StartCoroutine(Attack());
        }

        IEnumerator Attack()
        {
            while (true)
            {
                if (!UnitMover.IsMoving)
                {
                    var enemies = Main.EnemyController.Enemies.Where(c => IsInRange(c, 7));
                    foreach (var enemy in enemies)
                    {
                        var hit = Physics2D.Linecast(Position, enemy.Position, Layers.StaticMask);
                        if (hit.collider == null)
                        {
                            Shoot(enemy);
                            yield return new WaitForSeconds(0.3f);
                            break;
                        }
                    }
                }
                yield return null;
            }
        }

        void Shoot(Enemy enemy)
        {
            var shot = (GameObject) Instantiate(bullet, transform.position, Quaternion.identity);
            var bul = shot.GetComponent<Bullet>();
            bul.Fire(this, enemy.Position);
        }

        public void Stop()
        {
            UnitMover.Stop();
        }

        void DrawUnitCircles()
        {
            if (marineController.selected.Contains(this))
            {
                if (!selectCircle.activeSelf)
                {
                    selectCircle.SetActive(true);
                }
            }
            else
            {
                if (selectCircle.activeSelf)
                {
                    selectCircle.SetActive(false);
                }
            }
            if (marineController.selected.Contains(this) && UnitMover.IsMoving && UnitMover.Destination != (Vector2) transform.position)
            {
                if (!moveCircle.activeSelf)
                {
                    moveCircle.SetActive(true);
                }
            }
            else
            {
                if (moveCircle.activeSelf)
                {
                    moveCircle.SetActive(false);
                }
            }
            if ((Vector2) moveCircle.transform.position != UnitMover.Destination)
            {
                moveCircle.transform.position = UnitMover.Destination;
            }
        }

        public void MoveTo(Vector2 pos)
        {
            UnitMover.MoveTo(pos);
        }
    }
}