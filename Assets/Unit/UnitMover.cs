﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.Geometry;
using Game.Tiles;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof (Rigidbody2D))]
    [RequireComponent(typeof (CircleCollider2D))]
    public class UnitMover : GameBehaviour
    {
        const float Speed = 3;
        public Vector2 Destination;
        CircleCollider2D cc2D;
        Stack<Vector2> path;
        Vector2 waypoint;
        public bool IsMoving { get; private set; }

        void Awake()
        {
            cc2D = GetComponent<CircleCollider2D>();
        }

        Stack<Vector2> FindPath(Vector2 to, int[,] grid = null)
        {
            if (grid == null)
            {
                grid = Main.Map.Grid;
            }
            return new Pathfinder(grid, true).FindPath(Position, to);
        }

        public void Stop()
        {
            IsMoving = false;
            StopAllCoroutines();
        }

        IEnumerator MoveTowards()
        {
            while (waypoint != Position)
            {
                var velocity = (waypoint - Position).normalized*Speed;
                var hits = Physics2D.CircleCastAll(Position, cc2D.radius, velocity.normalized, velocity.magnitude*Time.deltaTime, Layers.DynamicMask);
                if (hits.Any(c => c.transform != transform))
                {
                    var unit = hits[0].transform.GetComponent<Unit>();
                    if (unit.IsFriendlyTo(GetComponent<Unit>()))
                    {
                        var mover = unit.UnitMover;
                        var conflict = new Conflict(this);
                        mover.ResolveConflict(conflict);
                        if (conflict.IsMovingAway)
                        {
                            yield return new WaitForSeconds(0.5f);
                            continue;
                        }
                    }
                    var wp = waypoint;
                    if (OverlapTile(wp))
                    {
                        if (path.Count > 0)
                        {
                            wp = path.Last();
                        }
                        while (path.Count > 0)
                        {
                            var v = path.Pop();
                            if (!OverlapTile(v))
                            {
                                wp = v;
                                break;
                            }
                        }
                    }
                    var dynamicPath = DynamicPath(wp);
                    if (dynamicPath == null)
                    {
                        yield break;
                    }
                    var dynPath = dynamicPath.Reverse().ToArray();
                    foreach (var v in dynPath)
                    {
                        path.Push(v);
                    }
                    waypoint = path.Pop();
                    yield break;
                }

                var angle = Mathf.Atan2(waypoint.y - Position.y, waypoint.x - Position.x)*Mathf.Rad2Deg - 90;
                transform.eulerAngles = new Vector3(0, 0, angle);
                transform.position = Vector2.MoveTowards(Position, waypoint, Speed*Time.deltaTime);
                yield return null;
            }
        }

        static Collider2D OverlapTile(Vector2 v)
        {
            var rect = Main.Tilemap.GetTile(v).Rect;
            var a = new Vector2(rect.X, rect.Y);
            var b = new Vector2(rect.X + rect.Width, rect.Y + rect.Height);
            var overlapArea = Physics2D.OverlapArea(a, b);
            return overlapArea;
        }

        Stack<Vector2> DynamicPath(Vector2 to)
        {
            var units = Physics2D.OverlapCircleAll(Position, 2, Layers.DynamicMask).Where(c => c.gameObject != gameObject).Select(c => c.GetComponent<UnitMover>()).ToArray();
            var tiles = units.SelectMany(c => c.OverlapTiles()).ToArray();
            var grid = (int[,]) Main.Map.Grid.Clone();
            foreach (var p in tiles)
            {
                grid[p.X, p.Y] = -1;
            }
            return new Pathfinder(grid, true).FindPath(Position, to);
        }

        public IEnumerable<Point> OverlapTiles()
        {
            var center = Main.Tilemap.GetTile(Position);
            var tiles = new List<Tile>(Main.Map.GetNeighbors(center)) {center};
            var circle = new Circle(cc2D.radius, Position);
            var rects = tiles.Select(c => c.Rect).ToArray();
            var enumerable = tiles.Where(n => circle.OverlapsRect(n.Rect)).ToArray();
            return enumerable.Select(c => c.Point);
        }

        void ResolveConflict(Conflict conflict)
        {
            if (IsMoving)
            {
                var dir = (waypoint - Position).normalized;
                var otherDir = (conflict.Other.Position - Position).normalized;
                var angle = Vector2.Angle(dir, otherDir);
                if (angle >= 90)
                {
                    conflict.IsMovingAway = true;
                }
            }
        }

        IEnumerator FollowPath()
        {
            Destination = path.Last();
            IsMoving = true;
            waypoint = path.Pop();
            Straighten(ref path);
            while (true)
            {
                if (waypoint == Position)
                {
                    if (path.Count > 0)
                    {
                        waypoint = path.Pop();
                        Straighten(ref path);
                    }
                    else
                    {
                        Stop();
                    }
                }
                yield return StartCoroutine(MoveTowards());
            }
        }

        void Straighten(ref Stack<Vector2> path)
        {
            while (path.Count > 0)
            {
                if (!PathObstructed(path.Peek()))
                {
                    waypoint = path.Pop();
                }
                else
                {
                    break;
                }
            }
        }

        public void MoveTo(Vector2 to)
        {
            Stop();
            if (!PathObstructed(to))
            {
                path = new Stack<Vector2>();
                path.Push(to);
                StartCoroutine(FollowPath());
                return;
            }
            //Stop if location is out of bounds, in a wall, or the starting location
            if (Main.Tilemap.InBounds(to) && Main.Tilemap.GetTile(to).IsPassable && Main.Tilemap.GetTile(to) != Main.Tilemap.GetTile(Position))
            {
                path = FindPath(to);
                if (path != null)
                {
                    StartCoroutine(FollowPath());
                }
            }
        }

        RaycastHit2D PathObstructed(Vector2 to)
        {
            var dir = to - Position;
            return Physics2D.CircleCast(Position, cc2D.radius, dir, dir.magnitude);
        }
    }
}