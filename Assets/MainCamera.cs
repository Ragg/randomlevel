﻿using System;
using UnityEngine;

namespace Game
{
    public class MainCamera : GameBehaviour
    {
        const float Speed = 3;
        public const float Aspect = 16.0f/9.0f;

        public void Move(Vector2 dir)
        {
            transform.Translate(dir*Speed*Time.deltaTime);
        }

        public void MoveTo(Vector2 pos)
        {
            transform.position = new Vector3(pos.x, pos.y, transform.position.z);
        }

        public void MoveToStart(Vector2 pos)
        {
            GetComponent<Camera>().orthographicSize = 4.725f;
            MoveTo(pos);
            CameraReady();
        }

        public event Action CameraReady = delegate { };

        void Start()
        {
            FixRes();
        }

        void FixRes()
        {
            // determine the game window's current aspect ratio
            var windowaspect = Screen.width/(float) Screen.height;

            // current viewport height should be scaled by this amount
            var scaleheight = windowaspect/Aspect;

            // if scaled height is less than current height, add letterbox
            if (scaleheight < 1.0f)
            {
                var rect = GetComponent<Camera>().rect;

                rect.width = 1.0f;
                rect.height = scaleheight;
                rect.x = 0;
                rect.y = (1.0f - scaleheight)/2.0f;

                GetComponent<Camera>().rect = rect;
            }
            else // add pillarbox
            {
                var scalewidth = 1.0f/scaleheight;

                var rect = GetComponent<Camera>().rect;

                rect.width = scalewidth;
                rect.height = 1.0f;
                rect.x = (1.0f - scalewidth)/2.0f;
                rect.y = 0;

                GetComponent<Camera>().rect = rect;
            }
        }
    }
}