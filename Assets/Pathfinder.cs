﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game
{
    class Pathfinder
    {
        readonly bool diagonal;
        readonly int[,] grid;

        public Pathfinder(int[,] grid, bool diagonal)
        {
            this.grid = grid;
            this.diagonal = diagonal;
            //DrawGrid(grid.GetLength(0) * grid.GetLength(1), grid.GetLength(0), grid.GetLength(1));
        }

        public Pathfinder(bool diagonal)
        {
            grid = MainController.Main.Map.Grid;
            this.diagonal = diagonal;
            //DrawGrid(grid.GetLength(0) * grid.GetLength(1), grid.GetLength(0), grid.GetLength(1));
        }

        void DrawGrid(int size, int sizeX, int sizeY)
        {
            var combine = new CombineInstance[size];
            var colors = new Color32[size*4];
            for (var x = 0; x < sizeX; x++)
            {
                for (var y = 0; y < sizeY; y++)
                {
                    //Create quad for merging into grid
                    var quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
                    quad.transform.position = new Vector3(x, y);
                    quad.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                    var filter = quad.GetComponent<MeshFilter>();
                    var i = x*sizeY + y;
                    combine[i].mesh = filter.mesh;
                    combine[i].transform = quad.transform.localToWorldMatrix;
                    Object.Destroy(quad);

                    //Specify vertex color
                    var vert = i*4;
                    for (var j = vert; j < vert + 4; j++)
                    {
                        if (grid[x, y] == 10)
                        {
                            colors[j] = Color.green;
                        }
                        else
                        {
                            colors[j] = Color.red;
                        }
                    }
                }
            }

            //Merge quads into one mesh
            var mesh = new Mesh();
            mesh.CombineMeshes(combine);
            var gridObject = new GameObject {name = "Grid"};
            gridObject.transform.Translate(0, 0, -1);
            var gridFilter = gridObject.AddComponent<MeshFilter>();
            gridFilter.mesh = mesh;

            //Fix renderer and material so that vertex colors will be used
            gridObject.AddComponent<MeshRenderer>();
            var sprite = new GameObject();
            sprite.AddComponent<SpriteRenderer>();
            gridObject.GetComponent<Renderer>().material = sprite.GetComponent<Renderer>().material;
            Object.Destroy(sprite);
            gridFilter.mesh.colors32 = colors;
        }

        public Stack<Point> FindPath(int x, int y, int xx, int yy)
        {
            var start = new Node(x, y);
            var goal = new Node(xx, yy);
            return CalcPath(start, goal);
        }

        public Stack<Vector2> FindPath(Vector2 from, Vector2 to)
        {
            var x = Mathf.FloorToInt(from.x);
            var y = Mathf.FloorToInt(from.y);
            var xx = Mathf.FloorToInt(to.x);
            var yy = Mathf.FloorToInt(to.y);
            IEnumerable<Point> points = FindPath(x, y, xx, yy);
            if (points == null)
            {
                return null;
            }
            var path = new Stack<Vector2>();
            path.Push(to);
            foreach (var p in points.ToArray().Reverse())
            {
                path.Push(new Vector2(p.X + 0.5f, p.Y + 0.5f));
            }
            return path;
        }

        IEnumerable<Node> Neighbors(Node n)
        {
            if (n.X + 1 < grid.GetLength(0) && Cost(n.X + 1, n.Y) > -1)
            {
                yield return new Node(n.X + 1, n.Y);
            }

            if (n.X - 1 >= 0 && Cost(n.X - 1, n.Y) > -1)
            {
                yield return new Node(n.X - 1, n.Y);
            }

            if (n.Y + 1 < grid.GetLength(1) && Cost(n.X, n.Y + 1) > -1)
            {
                yield return new Node(n.X, n.Y + 1);
            }

            if (n.Y - 1 >= 0 && Cost(n.X, n.Y - 1) > -1)
            {
                yield return new Node(n.X, n.Y - 1);
            }
            if (diagonal)
            {
                if (IsValid(n, 1, 1))
                {
                    yield return new Node(n.X + 1, n.Y + 1);
                }
                if (IsValid(n, 1, -1))
                {
                    yield return new Node(n.X + 1, n.Y - 1);
                }
                if (IsValid(n, -1, 1))
                {
                    yield return new Node(n.X - 1, n.Y + 1);
                }
                if (IsValid(n, -1, -1))
                {
                    yield return new Node(n.X - 1, n.Y - 1);
                }
            }
        }

        bool IsValid(Node n, int x, int y)
        {
            var xx = n.X + x;
            var yy = n.Y + y;
            return xx < grid.GetLength(0) &&
                   xx >= 0 &&
                   yy < grid.GetLength(1) &&
                   yy >= 0 &&
                   Cost(xx, yy) >= 0 &&
                   //Avoid cutting too close to corners
                   Cost(n.X, yy) >= 0 &&
                   Cost(xx, n.Y) >= 0;
        }

        float Cost(Node node)
        {
            return Cost(node.X, node.Y);
        }

        float Cost(int x, int y)
        {
            return grid[x, y];
        }

        float CostDiag(Node node)
        {
            return CostDiag(node.X, node.Y);
        }

        float CostDiag(int x, int y)
        {
            return grid[x, y]*Mathf.Sqrt(2);
        }

        //Finds a path using A* pathfinding
        Stack<Point> CalcPath(Node start, Node goal)
        {
            var closed = new Dictionary<Point, Node>();
            var open = new NodeQueue();
            open.Add(start.Point, start);
            while (open.Count > 0)
            {
                //Pick the node with the smallest F value
                var current = open.Min();

                //Construct and return the path
                if (current.IsEqual(goal))
                {
                    var path = new Stack<Point>();
                    var cur = current;
                    path.Push(cur.Point);
                    if (cur.Parent != null)
                    {
                        while (!cur.Parent.IsEqual(start))
                        {
                            path.Push(cur.Parent.Point);
                            cur = cur.Parent;
                        }
                        path.Push(cur.Parent.Point);
                    }
                    return path;
                }

                closed.Add(current.Point, current);
                foreach (var neighbor in Neighbors(current))
                {
                    //Don't need to check nodes on closed list because Manhattan Distance heuristic is consistent
                    if (!closed.ContainsKey(neighbor.Point))
                    {
                        if (open.ContainsKey(neighbor.Point))
                        {
                            var old = open[neighbor.Point];
                            if (current.G + Cost(neighbor) < old.G)
                            {
                                open.Remove(old.Point);
                                AddNode(goal, neighbor, current, open);
                            }
                        }
                        else
                        {
                            AddNode(goal, neighbor, current, open);
                        }
                    }
                }
            }
            return null;
        }

        void AddNode(Node goal, Node neighbor, Node current, NodeQueue open)
        {
            if (neighbor.X == current.X || neighbor.Y == current.Y)
            {
                neighbor.G = current.G + Cost(neighbor);
            }
            else
            {
                neighbor.G = current.G + CostDiag(neighbor);
            }
            neighbor.H = Heuristic(goal, current);
            neighbor.Parent = current;
            open.Add(neighbor.Point, neighbor);
        }

        float Heuristic(Node goal, Node node)
        {
            var dx = Mathf.Abs(goal.X - node.X);
            var dy = Mathf.Abs(goal.Y - node.Y);
            var D = 10;
            if (diagonal)
            {
                return D*(dx + dy) + (Mathf.Sqrt(2)*D - 2*D)*Mathf.Min(dx, dy);
            }
            return D*(dx + dy);
        }

        class Node
        {
            readonly Point point;
            public float G;
            public float H;
            public Node Parent;

            public Node(int x, int y)
            {
                point = new Point(x, y);
            }

            public int X
            {
                get { return point.X; }
            }

            public int Y
            {
                get { return point.Y; }
            }

            public Point Point
            {
                get { return point; }
            }

            public float F
            {
                get { return G + H; }
            }

            public override string ToString()
            {
                return "Node: (" + X + "," + Y + ")";
            }


            public bool IsEqual(Node other)
            {
                return X == other.X && Y == other.Y;
            }
        }

        class NodeQueue
        {
            readonly Dictionary<Point, Node> dict = new Dictionary<Point, Node>();
            readonly SortedList<float, List<Node>> sorted = new SortedList<float, List<Node>>();

            public int Count
            {
                get { return dict.Count; }
            }

            public Node this[Point key]
            {
                get { return dict[key]; }
            }

            public void Add(Point key, Node value)
            {
                dict.Add(key, value);
                if (!sorted.ContainsKey(value.F))
                {
                    sorted[value.F] = new List<Node>();
                }
                sorted[value.F].Add(value);
            }

            public void Remove(Point key)
            {
                var val = dict[key];
                if (sorted[val.F].Count == 1)
                {
                    sorted.Remove(val.F);
                }
                else
                {
                    sorted[val.F].Remove(val);
                }
                dict.Remove(key);
            }

            public bool ContainsKey(Point key)
            {
                return dict.ContainsKey(key);
            }

            public Node Min()
            {
                var e = sorted.GetEnumerator();
                e.MoveNext();
                var first = e.Current;
                var node = first.Value[0];
                Remove(node.Point);
                return node;
            }
        }
    }
}