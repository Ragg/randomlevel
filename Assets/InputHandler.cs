﻿using UnityEngine;

namespace Game
{
    class InputHandler : GameBehaviour
    {
        void LateUpdate()
        {
            var velocity = Vector2.zero;
            if (Input.GetKey(KeyCode.A))
            {
                velocity.x += -1;
            }
            if (Input.GetKey(KeyCode.D))
            {
                velocity.x += 1;
            }
            if (Input.GetKey(KeyCode.W))
            {
                velocity.y += 1;
            }
            if (Input.GetKey(KeyCode.S))
            {
                velocity.y += -1;
            }
            Main.MainCamera.Move(velocity.normalized);
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.F))
            {
                Main.MarineController.Stop();
            }
        }
    }
}