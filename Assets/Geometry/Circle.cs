﻿using UnityEngine;

namespace Game.Geometry
{
    public struct Circle
    {
        public readonly Vector2 Center;
        public readonly float Radius;

        public Circle(float radius, Vector2 center)
        {
            Radius = radius;
            Center = center;
        }

        public bool OverlapsRect(Rectangle rect)
        {
            var circleDistance = new Vector2();
            circleDistance.x = Mathf.Abs(Center.x - rect.Center.x);
            circleDistance.y = Mathf.Abs(Center.y - rect.Center.y);

            if (circleDistance.x > (rect.Width/2 + Radius))
            {
                return false;
            }
            if (circleDistance.y > (rect.Height/2 + Radius))
            {
                return false;
            }

            if (circleDistance.x <= (rect.Width/2))
            {
                return true;
            }
            if (circleDistance.y <= (rect.Height/2))
            {
                return true;
            }

            var cornerDistanceSq = Mathf.Pow(circleDistance.x - rect.Width/2, 2) +
                                     Mathf.Pow(circleDistance.y - rect.Height/2, 2);

            return (cornerDistanceSq <= (Mathf.Pow(Radius, 2)));
        }
    }
}