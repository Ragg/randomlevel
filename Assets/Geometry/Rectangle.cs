﻿using UnityEngine;

namespace Game.Geometry
{
    public struct Rectangle
    {
        public readonly float Height;
        public readonly float Width;
        public readonly float X;
        public readonly float Y;

        public Rectangle(float x, float y, float width, float height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public Vector2 Center
        {
            get { return new Vector2(X + Width/2, Y + Height/2); }
        }
    }
}