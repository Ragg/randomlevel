﻿using UnityEngine;

namespace Game
{
    public static class Layers
    {
        public const int Dynamic = 8;
        public const int Static = 9;
        public static readonly LayerMask DynamicMask = 1 << Dynamic;
        public static readonly LayerMask StaticMask = 1 << Static;
    }
}